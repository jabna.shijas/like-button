let like = 0;
const likeBtn = document.getElementById("like-btn");
const likesCount = document.getElementById("likes-count");
const heartIcon = document.getElementById("likeimg");

function addLike() {
    like += 1;
    likesCount.innerHTML = like + ' likes';
    
}

likeBtn.addEventListener('click', addLike);
